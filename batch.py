# coding:utf-8

from util.nlp_classifier import TextClassifier
from util.nlp_prcss import NlpProcess
from util.nlp_sentiment import NlpSentiment


def batch_prcss(in_file, nlp_file, sim_file):
    config = {'cws_model': './data/model/cws.model',
              'pos_model': './data/model/pos.model',
              'ner_model': './data/model/ner.model',
              'parser_model': './data/model/parser.model',
              'srl_dir': './data/model/src',
              'word_core': './data/word_core',
              'word_stop': './data/word_stop',
              'lexicon': './data/model/lexicon',
              'ner_stop': './data/ner_stop',
              'fenci_stop': './data/fenci_stop',
              'mobile_conf': './data/mobile',
              'chepai_conf': './data/che_shi_sheng',
              'b_words': './data/b_words',
              'ner_pos': './data/ner_pos',
              'id_card': './data/id_card',
              'bank_card': './data/bank_card',
              'conf_file': './conf/online.cfg',
              }
    nlp_prcss = NlpProcess(config)
    nlp_cls = TextClassifier()
    nlp_sent = NlpSentiment()

    texts = []
    count = 1
    with open(in_file, 'r') as fid, open(nlp_file, 'w') as gid:
        for line in fid:
            count += 1
            text = line.strip()
            texts.append(text)
            # words = nlp_prcss.get_words(text, punct_rm=True)
            (words, postags) = nlp_prcss.get_words_and_pos(text, punct_rm=True)
            # postags = nlp_prcss.get_words_pos(words)
            #ner_words = [i[0] for i in nlp_prcss.get_ner_words(words, postags)]
            ner_words=["%s:%s"%(key,",".join(value)) for key,value in nlp_prcss.get_ner_words_v2(words,postags).items() if len(value) > 0]
            ner_relation = nlp_prcss.get_ner_relation(text)
            key_words = nlp_prcss.get_key_words(text)
            key_sents = nlp_prcss.get_key_sentence(text)
            t_class = nlp_cls.regex_classifier(text,words)
            t_sent = nlp_sent.sentiment_level(text)
            gid.write('|'.join([
                text,
                "#".join(words),
                "#".join(ner_words),
                "#".join(ner_relation),
                "#".join(key_words),
                "#".join(key_sents),
                "#".join(t_class),
                # '#'.join(nlp_prcss.get_hot_words())
                "#".join(str(t_sent[0])),
            ]) + '\n')
            print("current line:%s" % count)

            # length = len(texts)
            # with open(sim_file, 'w') as sid:
            #     for num, txt in enumerate(texts):
            #         for i in range(num + 1, length):
            #             sid.write('|'.join([txt, texts[i],
            #                                 str(nlp_prcss.get_text_similarity(txt, texts[i]))]) + '\n')


if __name__ == "__main__":
    import sys

    in_file = sys.argv[1]
    nlp_file = sys.argv[2]
    sim_file = sys.argv[3]
    batch_prcss(in_file, nlp_file, sim_file)
