# coding:utf-8

"""
Author:songzhongsen
Date:2017-08-21
"""

import tornado.web
import tornado.ioloop
import tornado.httpserver
import os

from tornado.options import options,define

from util.nlp_prcss import NlpProcess
from util.nlp_sentiment import NlpSentiment
from util.nlp_classifier import TextClassifier
from util.mongo_util import MongoUtil
from handlers.text_nlp import TextNlp
from handlers.text_classification import TextClassification
from handlers.text_similarity_v2 import TextSimilarity
from handlers.text_words_search import TextWordSearch 
from handlers.data_feedback import DataFeedBack

define("deploy_env", default='test', help="语音助手部署环境")
define("port", default=8888, help="服务端口号")
define("debug", default=False, help="debug mode")


class ApplicationIA(tornado.web.Application):
    def __init__(self):
        url_patterns = [
            (r"/internal/text_nlp", TextNlp),
            (r"/internal/text_similarity",TextSimilarity),
            (r'/internal/text_similarity_v2',TextSimilarity),
            (r'/internal/text_words_search',TextWordSearch),
            (r'/internal/feedback',DataFeedBack),
            (r'/internal/text_classify',TextClassification)
            ]
        tornado.web.Application.__init__(self, handlers=url_patterns)

        deploy_env = './conf/test.cfg'
        if 'online'==options.deploy_env.lower():
            deploy_env = './conf/online.cfg'
        config = {
            'cws_model': './data/model/cws.model',
            'pos_model': './data/model/pos.model',
            'ner_model': './data/model/ner.model',
            'parser_model': './data/model/parser.model',
            'srl_dir': './data/model/src',
            'word_core': './data/word_core',
            'word_stop': './data/word_stop',
            'lexicon': './data/model/lexicon',
            'ner_stop': './data/ner_stop',
            'fenci_stop': './data/fenci_stop',
            'mobile_conf':'./data/mobile',
            'chepai_conf': './data/che_shi_sheng',
            'b_words': './data/b_words',
            'ner_pos': './data/ner_pos',
            'id_card': './data/id_card',
            'bank_card': './data/bank_card',
            'conf_file': deploy_env,
            }
        self.txt_prcss = NlpProcess(config)
        self.mongo_client = MongoUtil(deploy_env)
        self.txt_classifier = TextClassifier()
        self.txt_sent = NlpSentiment()

def main():
    options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(ApplicationIA())
    http_server.listen(options.port)
    print('server start ~~~', options.port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()

