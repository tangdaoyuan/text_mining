# coding:utf-8

"""
Author:songzhongsen
Date:2017-08-21
"""
import re

class ChePai(object):
    def __init__(self, conf_file='./data/che_shi_sheng'):
        self.conf_file =conf_file

        self.che_re = None
        self.che_shiqu_sheng = {}
        self.read_conf()
        self.re_chepai = re.compile('((?:%s)[A-Za-z0-9]+)'%'|'.join(self.che_shiqu_sheng.keys()))

    def read_conf(self):
        with open(self.conf_file, 'r') as fid:
            for line in fid:
                items = line.strip().split(',')
                if len(items) != 3:
                    continue
                self.che_shiqu_sheng[items[0]] = items[1:]

    def get_che_shengshi(self, che_pai):
        ret = {'sheng': '未知',
            'shiqu': '未知',
            'chepai':che_pai,
            'effective': False,
            }

        if che_pai[:2] in self.che_shiqu_sheng:
            ret['effective'] = True
            ret['sheng'] = self.che_shiqu_sheng[che_pai[:2]][1]
            ret['shiqu'] = self.che_shiqu_sheng[che_pai[:2]][0]

        return ret
    
    def is_chepai(self,string):
        return True if self.re_chepai.match(string) is not None and len(string) == 7  else False
