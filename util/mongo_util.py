#coding: utf-8

import configparser
from  motor.motor_tornado import MotorClient
class MongoUtil(object):
    def __init__(self,config_file):
        cf = configparser.ConfigParser()
        cf.read(config_file)

        hosts = cf.get("mongodb","hosts").split(",")
        ports = cf.get("mongodb","ports").split(",")

        self.mongo_client = MotorClient(("mongodb://%s" % (",".join(hosts))))
    
    @property
    def client(self):
        return self.mongo_client

    @property
    def db(self):
        return self.mongo_client['text_mining']
