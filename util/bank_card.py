# coding:utf-8

"""
Author:songzhongsen
Date:2017-08-21
"""


class BankCard(object):
    def __init__(self, conf_file='./data/bank_card'):
        self.conf_file = conf_file

        self.card_length = {16, 19}
        self.bank_card_dict = {}
        self.read_conf()

    def read_conf(self):
        with open(self.conf_file, 'r') as fid:
            for line in fid:
                items = line.strip().split(',')
                self.bank_card_dict[items[0]] = items[1]

    @staticmethod
    def luhn_algo(b_number):
        b_number = [int(i) for i in b_number]
        check_sum = sum(b_number[-2::-2]) + sum(map(lambda x: x if x < 10 else x - 9,
                                                    [2 * i for i in b_number[-3::-2]]))

        return True if 0 == check_sum % 10 else False

    def bank_card_check(self, bank_number):
        ret_dict = {'bank_agency': '未知',
                    'bank_card': bank_number,
                    'effective': False,
                    }
        if len(bank_number) not in self.card_length:
            return ret_dict

        if bank_number[:6] in self.bank_card_dict:
            if not self.luhn_algo(bank_number):
                return ret_dict
            ret_dict['bank_agency'] = self.bank_card_dict[bank_number[:6]]
            ret_dict['effective'] = True

        return ret_dict
