# coding:utf-8

"""
Author:songzhongsen
Date:2017-08-21
"""

class  MobilePrcss(object):
    def __init__(self, conf_file='./data/mobile'):
        self.conf_file = conf_file

        self.mobile_dict = {}
        self.virtual_operator = {('1700', '1701', '1702'): '中国电信',
            ('1703', '1705', '1706'): '中国移动',
            ('1704', '1707', '1708', '1709', '171'): '中国电信',
            }
        self.read_dict()

    def read_dict(self):
        with open(self.conf_file, 'r') as fid:
            for num,line in enumerate(fid,1):
                items = line.strip().split(',')
                if len(items)!=2:
                    print(items,num)
                    continue
                self.mobile_dict[items[0]] = items[1]

    def mobile_prcss(self, phone_num):
        ret_dict = {'mobile_operator': '未知', 
            'area': '未知',
            'phone':phone_num, 
            'effective': False,
            }
        if len(phone_num)!=11:
            return ret_dict

        m_opt = phone_num[:3]
        if '170'==m_opt:
            is_ok = False
            for kv,vl in self.virtual_operator.items():
                if phone_num[:4] in kv:
                    ret_dict['mobile_operator'] = vl
                    is_ok = True;break
            if not is_ok:
                return ret_dict
        elif m_opt in self.mobile_dict:
                ret_dict['mobile_operator'] = self.mobile_dict[m_opt]
        else:
            return ret_dict

        m_area = phone_num[:7]
        if m_area in self.mobile_dict:
            ret_dict['area'] = self.mobile_dict[m_area]
        else:
            return ret_dict

        ret_dict['effective'] = True
        return ret_dict
        

