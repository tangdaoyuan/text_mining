# coding:utf-8

from elasticsearch import Elasticsearch
import logging
class NlpSimilarity(object):
    def __init__(self,hosts,user=None,pwd=None):
        self.hosts = hosts
        self.index_name = "case_details"
        self.doc_type = "test-type"
        self.user = user
        self.pwd = pwd
        self.create_connection()
    
    def create_connection(self):
        if type(self.hosts) != list:
           logging.error("elasticsearch hosts must not be empty!")
        if self.user and self.pwd:
            self.es = Elasticsearch(self.hosts,http_auth=(self.user,self.pwd))
        else:
            self.es = Elasticsearch(self.hosts)

    def find_sim_case(self,ner_words,text_types):
        body = self.get_request_body()
        should = body['query']['bool']['should']

        for text_type in text_types['first_class']:
            should.append({"match_phrase":{"class":text_type}})

        for text_type in text_types['second_class']:
            should.append({"match_phrase":{"type":text_type}})
        
        for text_type in text_types['third_class']:
            should.append({"match_phrase":{"detail_type":text_type}})

        for item in ner_words.items():
            matches = [{"match_phrase":{"content":word}} \
                for word in item[1] if len(word) > 1]
            if len(matches) > 0:
                should.append({"bool":{"should":matches}})
        logging.info('find sim case request:{0}'.format(body))
        return self.es.search(index=self.index_name,doc_type=self.doc_type,body=body)     
    #key_words:list,
    #range_dates:{key:list},len(list)==2
    def find_key_case(self,key_words,range_dates={},minimum_should_match=3):
        body = self.get_highlight_body()
        # highlight
        #body['highlight']['fields']['content']={}

        # min frequency
        if len(key_words) < minimum_should_match:
            minimum_should_match = len(key_words)
        body['query']['bool']['minimum_should_match'] = minimum_should_match

        should = body['query']['bool']['should']
        must = body['query']['bool']['must']
        for key_word in key_words:
            should.append({"match_phrase":{"content":key_word}})
        for key,value in range_dates.items():
            if len(value) ==2:
                must.append({"range":{key:{"gte":value[0],"lte":value[1]}}})
        logging.info('find key case request:{0}'.format(body))
        return self.es.search(index=self.index_name,doc_type=self.doc_type,body=body)
    
    def get_highlight_body(self):
        body=self.get_request_body()
        body['highlight']={'fields':{}}
        return body

    def get_request_body(self):
        body = {
            "query":{
                "bool":{
                     "should":[],
                     "must":[]
                }
             }
        }
        return body
