# coding:utf-8

"""
Author:songzhongsen
Date:2017-08-21
"""


class IdCard(object):
    def __init__(self, conf_file='./data/id_card'):
        self.conf_file = conf_file

        self.card_province = {}
        self.weight = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]
        self.validate = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2']
        self.read_conf()

    def read_conf(self):
        with open(self.conf_file, 'r') as fid:
            for line in fid:
                items = line.strip().split(',')
                self.card_province[items[0]] = items[1]

    def idCard_prcss(self, card_number):
        ret_dict = {'jiguan': '未知',
                    'shengri': '未知',
                    'effective': False,
                    'id_card': card_number,
                    }
        if len(card_number) != 18:
            return ret_dict

        if card_number[:6] in self.card_province:
            if self.validate[sum([int(n) * self.weight[i] for i, n in enumerate( \
                    card_number[:17])]) % 11] != card_number[-1]:
                return ret_dict
            ret_dict['jiguan'] = self.card_province[card_number[:6]]
            ret_dict['shengri'] = card_number[6:14]
            ret_dict['effective'] = True

        return ret_dict
