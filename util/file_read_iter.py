# coding:utf-8

"""
Author:songzhongsen
Date:2017-08-21
"""

import os


class FileReadIter(object):
    def __init__(self):
        self.NOEXIST = -1

    def check_file(self, file_name):
        file_name = os.path.abspath(file_name)
        return self.NOEXIST if not os.path.exists(file_name) else file_name

    @staticmethod
    def read(file_name):
        with open(file_name, 'r') as fid:
            for line in fid:
                yield line

    @staticmethod
    def read_lineNum(file_name):
        with open(file_name, 'r') as fid:
            for line_num,line in enumerate(fid, 1):
                yield line_num,line

