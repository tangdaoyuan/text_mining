# coding:utf-8

import re
import traceback
from util import logger

class SameNameClassifier(object):
    def __init__(self):
        self.CN_NUM = {
                '一' : 1,
                '二' : 2,
                '三' : 3,
                '四' : 4, 
                '五' : 5, 
                '六' : 6, 
                '七' : 7, 
                '八' : 8, 
                '九' : 9
        }
        self.CN_UNIT = {
            '十' : 10,
            '百' : 100,
            '千' : 1000,
            '万' : 10000,
            '亿' : 100000000,
            '兆' : 1000000000000,
        }
        self.traditional_simplefied = {
            "几":"二",
            "两":"二",
            "壹":"一",
            "贰":"二",
            "叁":"三",
            "肆":"四",
            "伍":"五",
            "陆":"六",
            "柒":"七",
            "扒":"八",
            "玖":"九",
            "拾":"十",
            "佰":"百",
            "仟":"千"
        }

        self.re_filter = re.compile('[0-9零一二三四五六七八九十百千万几两壹贰叁肆伍陆柒扒玖拾佰仟]+(?:余元|余万元|万元|多元|元)+')
        self.re_cut = re.compile('[0-9一二三四五六七八九十百千万亿兆]+')
        self.re_digit = re.compile('[0-9]+[百千万亿兆]*')
        self.conditions=[\
                ["盗窃(治安)","盗窃(刑事)",400],\
                ["抢夺(治安)","抢夺(刑事)",2000],\
                ["诈骗(治安)","诈骗(刑事)",3000],\
                ["敲诈勒索(治安)","敲诈勒索(刑事)",3000],\
                ["恐吓(治安)","恐吓(刑事)",0],\
                ["制贩、使用假币(治安)","制贩、使用假币(刑事)",2000]]
       
    def transform(self,types,words):
        #人工优先规则
        if "群众投诉" in types:
            return ["群众投诉"]
        if "警务监督" in types:
            return ["警务监督"]
        if "提供线索" in types or "色情淫秽"  in types or "贩毒" in types or "组织、强迫、引诱、容留、介绍卖淫" in types :
            return ['提供线索']
        
        types = self.shot_most_important_type(['打杂哄抢','阻断交通','聚众上访','非法集会','交通秩序','治安纠纷','侵犯人身权利','扰乱秩序'],types)

        types = self.shot_most_important_type(['交通逃逸','交通违法','交通事故','交通秩序','交通保障'],types) 

        if "打架斗殴" in types and "侵犯人身权利" in types:
            types.remove("侵犯人身权利")

        try:
            for condition in self.conditions:
                if condition[0] in types and condition[1] in types:
                    types.remove(\
                            self.handle(words,condition)\
                            )
        except Exception:
            logger.error('error:{0}'.format(traceback.format_exc()))

        if "金融诈骗" in types: 
            if "诈骗(刑事)" in types:
                types.remove("诈骗(刑事)")
            else:
                types.remove("金融诈骗")
        return types

    def shot_most_important_type(self,clses,types):
        for ind,cls in enumerate(clses):
            if cls in types:
                for sub_ind in range(ind+1,len(clses)):
                    if clses[sub_ind] in types:types.remove(clses[sub_ind])
        return types

    def handle(self,words,condition):
        sums = 0
        for word in words:
            if self.re_filter.search(word):
                # remove chinese '零'
                word = word.replace('零','')
                #traditional to simplefied
                for k,v in self.traditional_simplefied.items():
                    word = word.replace(k,v)    

                re_result=self.re_cut.search(word).group()

                if type(re_result) == str and re_result.isdigit():
                    sums += int(re_result)
                else:
                    reg_results = self.re_digit.finditer(re_result)
                    for reg_result in reg_results:
                        reg_item = reg_result.group()
                        if reg_item.isdigit():
                            sums += int(reg_item)
                        elif len(reg_item)>1 and reg_item[:-1].isdigit():
                            sums += int(reg_item[:-1])*self.CN_UNIT.get(reg_item[-1])
                        re_result = re_result[0:reg_result.start()]\
                                + '@'*len(reg_item)\
                                + re_result[reg_result.end():]
                    re_result = re_result.replace("@","")

                    sums += self.chinese_to_arabic(re_result)
        return condition[1] if sums < condition[2] else condition[0]  

    def chinese_to_arabic(self,cn):
        unit = 0   # current
        prev = ''
        ldig = []  # digest
        for cndig in reversed(cn):
            if cndig in self.CN_UNIT:
                unit = self.CN_UNIT.get(cndig)
                if unit == 10000 or unit == 100000000:
                    ldig.append(unit)
                    unit = 1
            else:
                dig = self.CN_NUM.get(cndig)
                if prev in self.CN_NUM and cndig in self.CN_NUM:
                    prev = ''
                    continue
                if unit:
                    dig *= unit
                    unit = 0
                ldig.append(dig)
        if unit == 10:
            ldig.append(10)
        val, tmp = 0, 0
        for x in reversed(ldig):
            if x == 10000 or x == 100000000:
                val += tmp * x
                tmp = 0
            else:
                tmp += x
        val += tmp
        return val
