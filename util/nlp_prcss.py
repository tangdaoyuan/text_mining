# coding:utf-8

"""
Author:songzhongsen
Date:2017-08-21
"""

import re
import os
import configparser
import threading
import jieba
import Levenshtein
import jieba.analyse as analyse
import jieba.posseg as pseg

from collections import defaultdict, Counter

from pyltp import Parser
from pyltp import Segmentor
from pyltp import Postagger
from pyltp import SentenceSplitter
from pyltp import NamedEntityRecognizer
from pyltp import SementicRoleLabeller

from util.nlp_similarity import NlpSimilarity
from util.nlp_classifier import TextClassifier
from util.mobile_prcss import MobilePrcss
from util.che_pai import ChePai
from util.id_card import IdCard
from util.bank_card import BankCard
from util.jieba_to_ltp import Jieba2Ltp


class NlpProcess(object):
    def __init__(self, config):
        self.cws_model = config['cws_model']
        self.pos_model = config['pos_model']
        self.ner_model = config['ner_model']
        self.parser_model = config['parser_model']
        self.srl_dir = config['srl_dir']
        self.word_core = config['word_core']
        self.word_stop = config['word_stop']
        self.lexicon = config['lexicon']
        self.ner_stop = config['ner_stop']
        self.fenci_stop = config['fenci_stop']
        self.mobile_conf = config['mobile_conf']
        self.chepai_conf = config['chepai_conf']
        self.b_words = config['b_words']
        self.id_card = config['id_card']
        self.bank_card = config['bank_card']
        self.ner_pos_file = config['ner_pos']
        self.conf_file = config['conf_file']

        self.stop_word = {"，",
                          "。",
                          "：",
                          ":",
                          ",",
                          "、",
                          "~",
                          "！",
                          "`",
                          "#",
                          "￥",
                          "%",
                          " …",
                          "&",
                          "*",
                          "（",
                          "）",
                          "(",
                          ")",
                          "[",
                          "]",
                          "【",
                          "】",
                          "{",
                          "}",
                          "|",
                          "《",
                          "》",
                          ".",
                          "/",
                          "\\",
                          "[",
                          "]",
                          }
        self.word_split_punct = set(self.stop_word)
        self.text_cls = TextClassifier()
        self.phone_prcss = MobilePrcss(conf_file=self.mobile_conf)
        self.car_identify = ChePai(conf_file=self.chepai_conf)
        self.id_card_identify = IdCard(conf_file=self.id_card)
        self.bank_card_identify = BankCard(conf_file=self.bank_card)
        self.re_template = re.compile('((?:%s)[A-Za-z0-9]+)|(%s)' % (
            '|'.join(self.car_identify.che_shiqu_sheng.keys()),
            '[0-9零一二三四五六七八九十百千万几两壹贰叁肆伍陆柒扒玖拾佰仟]+(?:余元|余万元|多元|元)+'))
        self.re_position = re.compile('(%s)' % '[0-9一二三四五六七八九十]+(?:幢|栋|楼|单元|房|室)+')
        self.re_time = re.compile(r'(?:[0-9]{2,4}(?:年|\-|\/))(?:[0-9]{1,2}(?:月|\-|\/))(?:[0-9]{1,2}(?:日)*)')
        self.b_words_dict = {}
        self.ner_pos_dict = {}
        self.nlp_init()

    def nlp_init(self):
        self.segmentor = Segmentor()
        self.segmentor.load_with_lexicon(self.cws_model, self.lexicon)

        self.postagger = Postagger()
        self.postagger.load_with_lexicon(self.pos_model, self.lexicon)

        self.recognizer = NamedEntityRecognizer()
        self.recognizer.load(self.ner_model)

        self.parser = Parser()
        self.parser.load(self.parser_model)

        cf = configparser.ConfigParser()
        cf.read(self.conf_file)
        
        self.nlp_sim = NlpSimilarity(cf.get("elasticsearch","hosts").split(","))
        self.ner_pos = set(cf.get('ner_pos', 'ner_pos').split(','))

        with open(self.word_stop, 'r') as fid:
            for line in fid:
                self.stop_word.add(line.strip())

        self.ner_stop_dict = set()
        with open(self.ner_stop, 'r') as fid:
            for line in fid:
                self.ner_stop_dict.add(line.strip())

        self.fenci_stop_dict = set()
        with open(self.fenci_stop, 'r') as fid:
            for line in fid:
                self.fenci_stop_dict.add(line.strip())

        with open(self.b_words, 'r') as fid:
            for line in fid:
                self.b_words_dict[line.strip()] = 0

        with open(self.ner_pos_file, 'r') as fid:
            for line in fid:
                items = line.strip().split(',')
                self.ner_pos_dict[items[0]] = items[1]

        jieba.load_userdict(self.word_core)
        analyse.set_stop_words(self.word_stop)
        jieba.initialize()

        self.global_data = threading.local()

    @staticmethod
    def sent_split(text):
        return list(SentenceSplitter.split(text))

    def clear_words_cache(self):
        self.global_data.b_words_dict = self.b_words_dict.copy()

    def get_words(self, string, punct_rm=False):
        self.clear_words_cache()
        position_wds = [i for i in self.re_position.findall(string) ]
        cand_wds = [i for j in self.re_template.findall(string) for i in j if i]
        
        position_str=self.re_position.sub("WUHANGONGAN_POS",string)
        words = list(jieba.cut(self.re_template.sub("WUHANGONGAN", position_str)))
        count = 0
        pos_count=0
        for index, wd in enumerate(words):
            if words[index] in self.global_data.b_words_dict:
                #self.b_words_dict[words[index]] += 1
                self.global_data.b_words_dict[words[index].word] += 1
            if "WUHANGONGAN" == wd:
                words[index] = cand_wds[count]
                count += 1
            if "WUHANGONGAN_POS" == wd:
                words[index] = position_wds[pos_count]
                pos_count += 1
        if punct_rm:
            return [i for i in words if i not in self.word_split_punct]
        else:
            return list(words)

    def get_words_and_pos(self, string, punct_rm=False):
        self.clear_words_cache()
        position_wds = [i for i in self.re_position.findall(string)]
        time_wds = [i for i in self.re_time.findall(string)]
        cand_wds = [i for j in self.re_template.findall(string) for i in j if i]

        position_str = self.re_position.sub("WUHANGONGAN_POS",string)
        time_str = self.re_time.sub("WUHANGONGAN_TIME",position_str)
        words = list(pseg.cut(self.re_template.sub("WUHANGONGAN", time_str)))
        count = 0
        pos_count = 0
        time_count = 0
        for index, wd in enumerate(words):
            if words[index].word in self.global_data.b_words_dict:
                #self.b_words_dict[words[index].word] += 1
                self.global_data.b_words_dict[words[index].word] += 1 
            if "WUHANGONGAN" == wd.word:
                words[index].word = cand_wds[count]
                count += 1
            if "WUHANGONGAN_POS" == wd.word:
                words[index].word = position_wds[pos_count]
                pos_count += 1
            if "WUHANGONGAN_TIME" == wd.word:
                words[index].word = time_wds[time_count]
                time_count += 1

        if punct_rm:
            return [i.word for i in words if i.word not in self.word_split_punct and i.word.strip() != ''], \
                   [Jieba2Ltp.pos_mapping_by_flag(i.flag) for i in words if i.word not in self.word_split_punct and i.word.strip() != '']
        else:
            return [i.word for i in words if i.word.strip() != ''], \
                   [Jieba2Ltp.pos_mapping_by_flag(i.flag) for i in words if i.word.strip() != '']

    def get_hot_words(self, topk=5):
        return [ky for ky, cnt in sorted(self.global_data.b_words_dict.items(), \
                                         key=lambda x: x[1], reverse=True)[:topk] if cnt > 0]

    # def get_words_pos(self, words):
    #     return list(self.postagger.postag(words))

    def get_ner_words_v2(self,wds,postags,text,Tag=False):
        ret_ner = {
            "phone":[],
            "id_card":[],
            "bank_card":[],
            "organization_name":[],
            "che_pai":[],
            "location":[],
            "person_name":[],
            "date_time":[],
            "other":[],
        }
        re_digital = re.compile("[0-9]+")
        for ind,p in enumerate(postags):
            if wds[ind] not in self.ner_stop_dict:
                if 'm' == p:
                    postags[ind] = 'n'
                    if len(re_digital.findall(wds[ind])):
                        wds[ind] = re_digital.findall(wds[ind])[0]

                    if self.phone_prcss.mobile_prcss(wds[ind])['effective']:
                        ret_ner['phone'].append((wds[ind],ind))
                    elif self.id_card_identify.idCard_prcss(wds[ind])['effective']:
                        ret_ner['id_card'].append((wds[ind],ind))
                    elif self.bank_card_identify.bank_card_check(wds[ind])['effective']:
                        ret_ner['bank_card'].append((wds[ind],ind))
                elif p in self.ner_pos and wds[ind].strip() != '':
                    if 'ni' == p:
                        ret_ner['organization_name'].append((wds[ind],ind))
                    elif 'ns' == p:
                        ret_ner['location'].append((wds[ind],ind))
                    elif 'nh' == p and len(wds[ind]) > 1:
                        ret_ner['person_name'].append((wds[ind],ind))
                    elif 'nt' == p:
                        ret_ner['date_time'].append((wds[ind],ind))
                    elif self.car_identify.is_chepai(wds[ind]):
                        ret_ner['che_pai'].append((wds[ind],ind))
                    else:
                        ret_ner['other'].append((wds[ind],ind))

        if not Tag:
            return ret_ner,postags
        else:
            result = {kv:[(v[0],self.ner_pos_dict[postags[v[1]]]) for v in vl] for kv,vl in ret_ner.items()}
            return result,postags
        

    def get_ner_words(self, wds, postags, Tag=False):
        ret_ner = []
        for ind, p in enumerate(postags):
            if wds[ind] not in self.ner_stop_dict:
                if 'm' == p:
                    postags[ind] = 'n'
                    if self.phone_prcss.mobile_prcss(wds[ind])['effective']:
                        ret_ner.append((wds[ind], ind))
                    elif self.id_card_identify.idCard_prcss(wds[ind])['effective']:
                        ret_ner.append((wds[ind], ind))
                    elif self.bank_card_identify.bank_card_check(wds[ind])['effective']:
                        ret_ner.append((wds[ind], ind))
                    else:
                        postags[ind] = 'm'
                elif p in self.ner_pos and wds[ind].strip() != '':
                    ret_ner.append((wds[ind], ind))
        if not Tag:
            return ret_ner,postags
        else:
            return [(kv, self.ner_pos_dict[postags[vl]]) for kv, vl in ret_ner],postags

    def get_ner_relation(self, string):
        ner_list = []
        # words = self.get_words(string)
        # postags = self.get_words_pos(words)
        (words, postags) = self.get_words_and_pos(string)
        (ner_ind, postags) = self.get_ner_words(words, postags)
        arcs = [(ac.head, ac.relation) for ac in self.parser.parse(words, postags)]
        ind_set = {i[1] for i in ner_ind}
        for ner, ind in ner_ind:
            if arcs[ind][0] in ind_set and (arcs[ind][0] != ind):
                ner_list.append('&'.join([ner, words[arcs[ind][0]], arcs[ind][1]]))

        return ner_list

    def get_key_words(self, string, topk=6):
        position_wds = [i for i in self.re_position.findall(string)]
        time_wds = [i for i in self.re_time.findall(string)]
        cand_wds = [i for j in self.re_template.findall(string) for i in j if i]

        position_str=self.re_position.sub("WUHANGONGAN_POS",string)
        time_str = self.re_time.sub("WUHANGONGAN_TIME",position_str)
        replaced_str=self.re_template.sub("WUHANGONGAN", time_str)

        count = 0
        pos_count=0
        time_count = 0
        words = analyse.extract_tags(replaced_str, topK=topk)

        ret_list = []
        for item in words:
            if "WUHANGONGAN" == item:
                item = cand_wds[count]
                count += 1
            if "WUHANGONGAN_POS" == item:
                item = position_wds[pos_count]
                pos_count += 1 
            if "WUHANGONGAN_TIME" == item:
                item = time_wds[time_count] 
                time_count += 1
            if (not item.isdigit()) and (item not in self.fenci_stop_dict) and ('元' not in item):
                ret_list.append(item)

        return ret_list

    def get_key_sentence(self, text, topk=2):
        word_freq = [i[0] for i in sorted(Counter([wd for wd in jieba.cut(text) \
                                                   if
                                                   wd not in self.stop_word and wd not in self.word_split_punct]).items(), \
                                          key=lambda x: x[1], reverse=True)]
        sents = self.sent_split(text)
        k_sents = set()
        for wd in word_freq:
            for sent in sents:
                if re.search(wd, sent):
                    k_sents.add(sent)
                    break
            if len(k_sents) >= topk:
                break

        return list(k_sents)

    def get_text_similarity_v2(self,text):
        (words,postags) = self.get_words_and_pos(text)
        (ner_words,_) = self.get_ner_words_v2(words,postags,text)
        text_types = self.text_cls.regex_classifier(text,words)

        ner_words = { item[0]:[ word[0] for word in item[1] if word ] \
            for item in ner_words.items() }
        return self.nlp_sim.find_sim_case(ner_words,text_types)['hits'] 

    def __del__(self):
        try:
            self.segmentor.release()
            self.postagger.release()
            self.recognizer.release()
            self.parser.release()
        except Exception:
            pass
