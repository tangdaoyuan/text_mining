# coding:utf-8

"""
Author:songzhongsen
Date:2017-08-21
"""

import re
import yaml

from util.classifier.classifier_util import SameNameClassifier

class TextClassifier(object):
    def __init__(self, conf_file='./conf/second_classifier.yaml',
            extra_conf_file='./conf/third_classifier.yaml',
            synonym_file='./conf/synonym.yaml',
            s2f_mapping_file = './conf/s2f_mapping.yaml'):
        self.conf_file = conf_file
        self.extra_conf_file = extra_conf_file
        self.synonym_file = synonym_file

        self.cls_regex = {}
        self.ex_cls_regex = {}
        self.synonym_dict = {}
        self.s2f_dict = {}
        self.t2s_dict = {}
        self.read_conf()
        self.same_name_class = SameNameClassifier()

    def read_conf(self):
        with open(self.conf_file, 'r') as fid:
            cls_dict = yaml.load(fid)

        for kw,vl in cls_dict.items():
            self.cls_regex[kw] = re.compile(vl)

        with open(self.extra_conf_file,'r') as fid:
            extra_cls_dict = yaml.load(fid)
        
        for kw,vl in extra_cls_dict.items():
            self.t2s_dict[kw] = list(vl.keys())
            for e_kw,e_vl in vl.items():
                self.ex_cls_regex[e_kw] = re.compile(e_vl)


        with open(self.synonym_file, 'r') as fid:
           synonym = yaml.load(fid)

        for ky,vl in synonym.items():
            self.synonym_dict[ky] = re.compile(vl)
        
        with open('./conf/s2f_mapping.yaml') as fid:
            s2f = yaml.load(fid)

        for kw,vl in s2f.items():
            for s in vl.split('|'):
                self.s2f_dict[s] = kw

    def string_sub(self, string):
        new_str = string
        for ky,vl in self.synonym_dict.items():
            if vl.search(string):
                new_str = re.sub(vl, ky, string)

        return new_str

    def regex_classifier(self, text, words):
        ret_list = []
        string = self.string_sub(text)
        for cls,rg in self.cls_regex.items():
            if rg.search(string):
                ret_list.append(cls)

        ret_list = self.same_name_class.transform(ret_list,words)

        seconds = ret_list if ret_list else ['其他报警类型']
        first_list = [ self.s2f_dict[ret] for ret in ret_list]
        third_list = []

        #for s_cls in seconds:
        #    if s_cls in list(self.t2s_dict.keys()):
        #        ret = '其它'
        #        for kv in self.t2s_dict[s_cls]:
        #            if self.ex_cls_regex[kv].search(string):
        #                ret = kv
        #        third_list.append(ret)      
        #    else:
        #        third_list.append('')
        
        second_list = []
        print(seconds)
        for s in seconds:
            if '(' in s:
                second_list.append(s[:s.find("(")])
            else:
                second_list.append(s)

        return  {
                    'first_class':first_list,\
                    'second_class':second_list,\
                    'third_class':third_list
                }


