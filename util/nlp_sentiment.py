# coding:utf-8

"""
Author:songzhongsen
Date:2017-08-21
"""

import re
import os
import jieba

from itertools import chain
 
from util.file_read_iter import FileReadIter


class NlpSentiment(FileReadIter):
    def __init__(self, conf_dir='./data/sentiment'):
        super(NlpSentiment, self).__init__()

        self.conf_dir = conf_dir
        self.load_dict()

        self.re_sp = re.compile('[\s\u3000]+')
        self.re_han = re.compile("[\u4E00-\u9FD5a-zA-Z0-9+#&._]+", re.U)
        self.sent_split = re.compile('[。？！……——]')

        self.tokenizer = jieba.Tokenizer()
        for wd in chain(self.degree.keys(), self.invert, self.user_dict):
            self.tokenizer.add_word(wd)

    def load_dict(self):
        user_pos = self.read_conf('p', 0.2)
        user_neg = self.read_conf('n', 0.2)

        self.user_dict = set()
        self.user_dict.update(user_pos.keys())
        self.user_dict.update(user_neg.keys())

        extra_pos = self.read_conf('zp', False)
        extra_neg = self.read_conf('zn', False)

        self.weights = {}
        self.weights.update((k, 0.1) for k in extra_pos)
        self.weights.update((k, -0.1) for k in extra_neg)
        self.weights.update(user_pos)
        self.weights.update(user_neg)

        self.degree = self.read_conf('d', 1.0)

        self.invert = self.read_conf('i', False)

    def read_conf(self, file_name, weight):
        if weight:
            result = {}
            for line in self.read(os.path.join(self.conf_dir, file_name)):
                items = line.strip().split()
                if len(items) != 2:
                    continue
                result[items[0]] = float(items[1])*weight
        else:
            result = set()
            for line in self.read(os.path.join(self.conf_dir, file_name)):
                result.add(line.strip())

        return result

    def sent_polarity(self, sentence, trunc=300):
        words = [w for w in self.tokenizer.cut(self.re_sp.sub('', sentence)) \
            if self.re_han.match(w)]

        sentence_weight = []
        sentence_invert = []
        sentence_degree = []
        for i, w in enumerate(words):
            if w in self.weights:
                sentence_weight.append([i, self.weights[w]])
            elif w in self.invert:
                sentence_invert.append(i)
            elif w in self.degree:
                sentence_degree.append((i, self.degree[w]))

        for pos1 in sentence_invert:
            for index, (pos2, weight) in enumerate(sentence_weight):
                if 0<pos2-pos1<4:
                    sentence_weight[index][1] *= -1.0
                    break
                elif pos2-pos1>4:
                    break
        
        for pos1, modifier in sentence_degree:
            for index, (pos2, weight) in enumerate(sentence_weight):
                if abs(pos1 - pos2) < 3:
                    sentence_weight[index][1] *= modifier
                    break

        score = sum(x for _, x in sentence_weight)

        if len(words) < 5 and len(sentence_weight) == 1:
            score *= 3.0

        return score

    def polarity_avg(self, text):
        sents = re.split(self.sent_split, text)
        return sum([self.sent_polarity(s) for s in sents])/float(len(sents))

    def sentiment_level(self, text):
        score = self.polarity_avg(text)
        if score>0.55:
            return (1, score)
        elif score<-0.4:
            return (-1, score)
        return (0, score)
