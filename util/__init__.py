# coding:utf-8

"""
Author:songzhongsen
Date:2017-08-21
"""
import os
import logging
import logging.config

logger_file = "./conf/logger.ini"
logger_handler = "single_file"
logging.config.fileConfig(logger_file)
logger = logging.getLogger(logger_handler)
