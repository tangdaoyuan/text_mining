FROM registry.docker-cn.com/library/python:3.6

COPY requirements.txt ./
RUN pip install --no-cache-dir -i http://172.17.0.1:8082 --trusted-host 172.17.0.1 -r requirements.txt
COPY . .
CMD [ "python", "./app.py" ]
