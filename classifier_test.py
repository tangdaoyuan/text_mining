#coding: utf-8

from elasticsearch import Elasticsearch
from configparser import ConfigParser
from pymongo import MongoClient
from util.nlp_prcss import NlpProcess
from util.nlp_classifier import TextClassifier
from util import logger

cf = ConfigParser()
cf.read("./conf/test.cfg")
#init mongo client
mongo_hosts = cf.get("mongodb","hosts").split(",")
client = MongoClient("mongodb://%s"%(",".join(mongo_hosts)))
collection = client['text_mining']['text_classifier']
#elasticsearch init
es_hosts = cf.get("elasticsearch","hosts").split(",")

es = Elasticsearch(es_hosts)
es_index_name = "case_details"
es_doc_type = "test-type"

config = {'cws_model': './data/model/cws.model',
              'pos_model': './data/model/pos.model',
              'ner_model': './data/model/ner.model',
              'parser_model': './data/model/parser.model',
              'srl_dir': './data/model/src',
              'word_core': './data/word_core',
              'word_stop': './data/word_stop',
              'lexicon': './data/model/lexicon',
              'ner_stop': './data/ner_stop',
              'fenci_stop': './data/fenci_stop',
              'mobile_conf': './data/mobile',
              'chepai_conf': './data/che_shi_sheng',
              'b_words': './data/b_words',
              'ner_pos': './data/ner_pos',
              'id_card': './data/id_card',
              'bank_card': './data/bank_card',
              'conf_file': './conf/test.cfg',
        }
txt_prcss = NlpProcess(config)
txt_classifier = TextClassifier()

count = es.count(index=es_index_name)['count']
size = 10000
pages = count//size


debug_file = open('./test_classifiler_debug','w')

for page in range(0,pages):
    if page == 0:
        rets = es.search(index=es_index_name,doc_type=es_doc_type,sort=["id:asc"],size=size,scroll='1m')
    else:
        rets = es.scroll(scroll='2m',scroll_id=rets['_scroll_id']) 

    print("current page:%s" % page)
    hits = rets['hits']['hits']
    for ret in hits:
        (words,postags) = txt_prcss.get_words_and_pos(ret['_source']['content'],punct_rm=True)
        try:
            t_class=txt_classifier.regex_classifier(ret['_source']['content'],words)
            if len(t_class) > 1:
                #mongo insert
                collection.insert_one({'content':ret['_source']['content'],'t_class':t_class,'s_class':ret['_source']['type'],'s_type':ret['_source']['class']})
        except Exception:
            debug_file.write('%s\n'%ret['_source']['content'])
            logger.error("conetent:{0},error:{1}".format(ret['_source']['content'],traceback.format_exc()))

debug_file.close()

