# coding:utf-8

import json
import traceback
from util import logger
from tornado.web import asynchronous
from tornado.gen import coroutine
from handlers.base import BaseHandler

class DataFeedBack(BaseHandler):
    
    @asynchronous
    @coroutine
    def post(self):
        ret = {'code': -1, 'msg': 'failed'}
        try:
            req = json.loads(self.request.body)
            result = yield self.application.mongo_client.db['feedback'].insert_one({\
                    'content':req['content'],\
                    'suggestion':req['suggestion'],\
                    'result':req['result'],\
                    'accept':req['accept']
                })
            ret = {'code': 0 ,'msg':'success'}
        except Exception:
            logger.error('data_feedback,body:{0},error:{1}'.format(
                self.request.body.decode('utf-8'), traceback.format_exc()))
        self.write(json.dumps(ret))
