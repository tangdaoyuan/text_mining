# coding:utf-8

import time
import json
import traceback

from tornado.web import asynchronous
from tornado.gen import coroutine
from tornado.concurrent import run_on_executor
from concurrent.futures import ThreadPoolExecutor

from util import logger
from handlers.base import BaseHandler


class TextSimilarity(BaseHandler):
    executor = ThreadPoolExecutor(64)

    @asynchronous
    @coroutine
    def post(self):
        result = yield self.sync_process(self.request.body, self.application.txt_prcss)
        self.write(result)

    @run_on_executor
    def sync_process(self, query, s_fuc):
        ret = {'code': -1, 'msg': 'failed'}
        
        st = time.time()
        try:
            q = json.loads(self.request.body)
            ret = {'code': 0, 'msg': s_fuc.get_text_similarity(q['str1'], q['str2'])}
        except Exception:
            logger.error('nlp_similarity,body:{0},error:{1}'.format(
                self.request.body.decode('utf-8'), traceback.format_exc()))
        total_time = (time.time()-st)*1000
        logger.info('request:{0}, ia main costtime(ms):{1}'.format(
            self.request.body, total_time))

        return json.dumps(ret)

