# coding:utf-8

"""
Author:songzhongsen
Date:2017-08-21
"""

import json
import tornado.web


class BaseHandler(tornado.web.RequestHandler):
    def load_json(self):
        try:
            self.request.arguments = json.loads(self.request.body)
        except ValueError:
            msg = "Could not decode JSON: %s" % self.request.body
            raise tornado.web.HTTPError(400, msg)
