# coding:utf-8

import time
import json
import traceback

from tornado.web import asynchronous
from tornado.gen import coroutine
from tornado.concurrent import run_on_executor
from concurrent.futures import ThreadPoolExecutor

from util import logger
from handlers.base import BaseHandler


class TextClassification(BaseHandler):
    executor = ThreadPoolExecutor(64)

    @asynchronous
    @coroutine
    def post(self):
        result = yield self.sync_process(self.request.body,
                                         self.application.txt_prcss,
                                         self.application.txt_sent,
                                         self.application.txt_classifier,
                                         )
        self.write(result)

    @run_on_executor
    def sync_process(self, text, nlp_prcss, nlp_sent, nlp_cls):
        ret = {'code': -1, 'msg': 'failed'}

        st = time.time()
        try:
            text = json.loads(self.request.body)['string']
            #words = nlp_prcss.get_words(text, punct_rm=True)
            #postags = nlp_prcss.get_words_pos(words)
            (words,postags) = nlp_prcss.get_words_and_pos(text,punct_rm=True)
            ret = {
                    'code': 0,
                    'msg': {
                       't_class': nlp_cls.regex_classifier(text,words)
                        }
                   }
        except Exception:
            logger.error('nlp_classification,body:{0},error:{1}'.format(
                self.request.body.decode('utf-8'), traceback.format_exc()))
        total_time = (time.time() - st) * 1000
        print(ret)

        logger.info('request:{0}, ia main costtime(ms):{1}'.format(
            self.request.body, total_time))

        return json.dumps(ret)
