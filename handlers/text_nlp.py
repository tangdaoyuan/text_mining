# coding:utf-8

import time
import json
import traceback

from tornado.web import asynchronous
from tornado.gen import coroutine
from tornado.concurrent import run_on_executor
from concurrent.futures import ThreadPoolExecutor

from util import logger
from handlers.base import BaseHandler


class TextNlp(BaseHandler):
    executor = ThreadPoolExecutor(64)

    @asynchronous
    @coroutine
    def post(self):
        result = yield self.sync_process(self.request.body,
                                         self.application.txt_prcss,
                                         self.application.txt_sent,
                                         self.application.txt_classifier,
                                         )
        self.write(result)

    @run_on_executor
    def sync_process(self, text, nlp_prcss, nlp_sent, nlp_cls):
        ret = {'code': -1, 'msg': 'failed'}

        st = time.time()
        try:
            text = json.loads(self.request.body)['string']
            #words = nlp_prcss.get_words(text, punct_rm=True)
            #postags = nlp_prcss.get_words_pos(words)
            (words,postags) = nlp_prcss.get_words_and_pos(text,punct_rm=True)
            ner_words,_ = nlp_prcss.get_ner_words_v2(words,postags,text,Tag=True)
            ret = {'code': 0,
                   'msg': {'ner_words': ner_words,
                           'words': words,
                           'hot_words': nlp_prcss.get_hot_words(),
                           'ner_relation': nlp_prcss.get_ner_relation(text),
                           'key_words': nlp_prcss.get_key_words(text),
                           'key_sents': nlp_prcss.get_key_sentence(text),
                           't_class': nlp_cls.regex_classifier(text,words),
                           't_sentiment': nlp_sent.sentiment_level(text)
                           }
                   }
        except Exception:
            logger.error('nlp_similarity,body:{0},error:{1}'.format(
                self.request.body.decode('utf-8'), traceback.format_exc()))
        total_time = (time.time() - st) * 1000
        print(ret)

        logger.info('request:{0}, ia main costtime(ms):{1}'.format(
            self.request.body, total_time))

        return json.dumps(ret)
