# coding: utf-8
from elasticsearch import Elasticsearch 
from elasticsearch import helpers
from configparser import ConfigParser
from argparse import ArgumentParser
import traceback
import os
import pandas as pd 
import time 
import logging

parser = ArgumentParser(description="pour data command line arguments")
parser.add_argument("--deploy_env",type=str,default="test")
parser.add_argument("--data_dir",type=str,default='/Users/tangdaoyuan/Desktop/data')
parser.add_argument("--input_file",type=str)
parser.add_argument("--input_type",type=str,default="xlsx")

args = parser.parse_args()
if __name__ == "__main__":
   
    deploy_env = args.deploy_env
    data_dir = args.data_dir
    input_file = args.input_file
    input_type = args.input_type
    print("deploy_env:{0},data_dir:{1}".format(deploy_env,data_dir))

    if not deploy_env and not data_dir and input_file:
        logging.error("can not find deploy flag and data_dir and input file!")
        exit(0)
  
    if input_file:
        if input_type == "xlsx":
            df = pd.read_excel(input_file)
        elif input_type == "csv":
            df = pd.read_csv(input_file,sep = "/001")
    else:
        dfs = []
        for name in os.listdir(data_dir):
            if str.endswith(name,"xlsx") and not  str.startswith(name,"~"):
                dfs.append(\
                        pd.read_excel(os.path.join(data_dir,name))\
                        )
        df = pd.concat(dfs,ignore_index=True)

    DEFAULT_MISS_VALUE=0
    DEFAULT_MISS_STR=''
    df = pd.DataFrame({
                'content':df['报警内容'],\
                'sex':df['报警人性别'],\
                'class':df['报警类别名称'],\
                'type':df['报警类型名称'],\
                'detail_type':df['报警类型细类名称'].fillna(DEFAULT_MISS_STR),\
                'assign_date':df['实际出警时间'].fillna(DEFAULT_MISS_VALUE),\
                'feedback_date':df['反馈时间'].fillna(DEFAULT_MISS_VALUE),\
                'call_date':df['呼入时间'].fillna(DEFAULT_MISS_VALUE),\
                'arrival_date':df['到达现场时间'].fillna(DEFAULT_MISS_VALUE),\
                'informer_phone':df['报警电话'].fillna(DEFAULT_MISS_STR),\
                'informer_phone2':df['报警人联系电话'].fillna(DEFAULT_MISS_STR),\
                'happening_place':df['案发地点'].fillna(DEFAULT_MISS_STR),\
                'happening_place1':df['案发地点1'].fillna(DEFAULT_MISS_STR),\
                'precinct_organization':df['管辖单位名称'].fillna(DEFAULT_MISS_STR),\
                'precinct_area':df['管辖区域名称'].fillna(DEFAULT_MISS_STR),\
                'feedback_area':df['反馈单位名称'].fillna(DEFAULT_MISS_STR),\
                'receive_class':df['接警类别名称'].fillna(DEFAULT_MISS_STR),\
                'receive_method':df['接警方式名称'].fillna(DEFAULT_MISS_STR),\
                'receive_number':df['接警单编号'].fillna(DEFAULT_MISS_STR),\
                'receive_duration':df['接警时间按小时'].fillna(DEFAULT_MISS_STR),\
                'deal_number':df['处警单编号'].fillna(DEFAULT_MISS_STR),\
                'case_address_class':df['案发地址类别名称'].fillna(DEFAULT_MISS_STR),\
                'case_address_type':df['案发地址类型名称'].fillna(DEFAULT_MISS_STR),\
                'case_address_route':df['案发区路'].fillna(DEFAULT_MISS_STR),\
                'call_serial_number':df['呼叫流水号'].fillna(DEFAULT_MISS_STR),\
                'case_address_route_unique':df['案发区路去重'].fillna(DEFAULT_MISS_STR),\
                'receiver_deal_type':df['接警单处理类型名称'].fillna(DEFAULT_MISS_STR),\
                'feedback_number':df['反馈单编号'].fillna(DEFAULT_MISS_STR),\
                'feedback_orgnization':df['反馈单位名称'].fillna(DEFAULT_MISS_STR),\
                'feedback_organization_number':df['反馈单位代码'].fillna(DEFAULT_MISS_STR),\
                'assign_desc':df['出警情况'].fillna(DEFAULT_MISS_STR),\
                'x_axis':df['X坐标'].fillna(DEFAULT_MISS_VALUE),\
                'y_axis':df['Y坐标'].fillna(DEFAULT_MISS_VALUE)})
    df = df.dropna(how='any').reset_index(drop=True)

    es_index_name = "case_details"
    es_doctype_name = "test-type"

    conf_path = "../conf/test.cfg"
    if deploy_env == "online":
        conf_path = "../conf/online.cfg"

    cf = ConfigParser()
    cf.read(conf_path)
    hosts=cf.get("elasticsearch",'hosts').split(",")
    logging.info("hosts:{0}".format(hosts))
    es = Elasticsearch(hosts,timeout=600,request_timeout=600,max_retries=10,retry_on_timeout=True)
   
    if es.indices.exists(index=es_index_name):
        start_index = es.search(index=es_index_name,doc_type=es_doctype_name,body={
            "aggs" : {
                 "max_id" : { "max" : { "field" : "id" } }
             }
        })['aggregations']['max_id']['value']
        start_index = 0 if not start_index else int(start_index) 
    else:
        start_index = 0 
        es.indices.create(es_index_name,ignore=400)

    def date_format(s):
        if type(s) == int and s == DEFAULT_MISS_VALUE:
            return s
        return int(time.mktime(time.strptime(s,'%Y-%m-%d %H:%M:%S')))

    def create_action(index,body):
        return {
            '_op_type': 'create',
            '_index': es_index_name,
            '_type': es_doctype_name,
            '_id': body['id'],
            '_source': body
        }
    count=0
    while not es.indices.exists(index=es_index_name) and count < 10:
        print("index {0} not found!".format(es_index_name))
        count +=1
    #for writting data faster 
    es.indices.put_settings(index=es_index_name,body={\
            'index':{
                    "refresh_interval":-1,
                    "number_of_replicas":0
                }
        })    
    
    actions = []
    for index,row in df.iterrows():
        row['content'] = row['content'].replace("\n","")
        row['assign_date'] = date_format(row['assign_date'])
        row['feedback_date'] = date_format(row['feedback_date'])
        row['call_date'] = date_format(row['call_date'])
        row['arrival_date'] = date_format(row['arrival_date'])
        
        if index % 10000 == 0:
            if len(actions) > 0: 
                for success, info in helpers.parallel_bulk(es,chunk_size=1000,actions,thread_count=4):
                    if not success:
                        logger.error("create doc failed:%s" % info)
            del actions
            actions = []
            print("current:%d" % (int(start_index) + int(index)+1))
        
        body = row.to_dict() 
        body['id'] = int(start_index) + int(index) + 1
        actions.append(create_action(index,body))

    # recover index
    es.indices.put_settings(index=es_index_name,body={
            'index':{
                    "refresh_interval":"10s",
                    "number_of_replicas":1
                }    
        })
