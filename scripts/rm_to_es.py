# coding: utf-8
from elasticsearch import Elasticsearch
from configparser import ConfigParser
from argparse import ArgumentParser
import logging

parser = ArgumentParser(description="remove data command line arguments")
parser.add_argument("--deploy_env",type=str,default="test")
args = parser.parse_args()
if __name__ == "__main__":
   
    deploy_env = args.deploy_env
    print("deploy_env:{0}".format(deploy_env))

    if not deploy_env:
        logging.error("can not find deploy flag!")
        exit(0)

    es_index_name = "case_details"
    es_doctype_name = "test-type"

    conf_path = "../conf/test.cfg"
    if deploy_env == "online":
        conf_path = "../conf/online.cfg"
    cf = ConfigParser()
    cf.read(conf_path)
    hosts=cf.get("elasticsearch",'hosts').split(",")
    logging.info("hosts:{0}".format(hosts))
    es = Elasticsearch(hosts)

    es.indices.delete(index=es_index_name)
